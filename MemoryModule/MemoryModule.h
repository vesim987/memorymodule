#pragma once

#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>


class MemoryModule
{
public:
	MemoryModule(void);
	~MemoryModule(void);

	int Load(unsigned char *mem);
	FARPROC GetProcAddress(char* name);
	HMODULE GetHandle();

private:
	PIMAGE_NT_HEADERS headers;
	unsigned char *code;
	bool initialized;
	std::vector<PIMAGE_SECTION_HEADER> sections;
	DWORD RVAToOffset(DWORD dwRVA);

};

