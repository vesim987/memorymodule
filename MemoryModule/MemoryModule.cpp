#include "stdafx.h"
#include "MemoryModule.h"

FARPROC GetFuncAddr(HMODULE handle, LPCSTR name)
{
	return GetProcAddress(handle, name);
}

MemoryModule::MemoryModule(void)
{
	initialized = false;
}


MemoryModule::~MemoryModule(void)
{
	//if(initialized)
	{
		puts("calling");
		((BOOL (WINAPI *)(HINSTANCE, DWORD, LPVOID))(code + headers->OptionalHeader.AddressOfEntryPoint))((HINSTANCE)code, DLL_PROCESS_DETACH, 0);
	}
}

DWORD MemoryModule::RVAToOffset(DWORD dwRVA)
{
	DWORD dwValue = dwRVA;

	for (int i = 0; i < sections.size(); i++)
	{
		if ((dwRVA >= sections[i]->PointerToRawData) & (dwRVA < sections[i]->VirtualAddress + sections[i]->Misc.VirtualSize))
		{
			dwValue = (dwRVA - sections[i]->VirtualAddress) + sections[i]->PointerToRawData;
			break;
		}
	}
	return dwValue;
}

int MemoryModule::Load(unsigned char *mem)
{
#pragma region HEADERS

	PIMAGE_DOS_HEADER dosHeader;
	PIMAGE_NT_HEADERS ntHeader;

	dosHeader = (PIMAGE_DOS_HEADER)mem;

	if(dosHeader->e_magic != IMAGE_DOS_SIGNATURE)
	{
		return 1;
	}

	ntHeader = (PIMAGE_NT_HEADERS)&mem[dosHeader->e_lfanew];
	headers = (PIMAGE_NT_HEADERS)malloc(sizeof(IMAGE_NT_HEADERS));

	if(ntHeader->Signature != IMAGE_NT_SIGNATURE)
	{
		return 2;
	}

	code = (unsigned char*)VirtualAlloc((LPVOID)ntHeader->OptionalHeader.ImageBase, 
		ntHeader->OptionalHeader.SizeOfImage,
		MEM_RESERVE | MEM_COMMIT,
		PAGE_READWRITE);

	if(!code)
	{
		code = (unsigned char*)VirtualAlloc(NULL, 
			ntHeader->OptionalHeader.SizeOfImage,
			MEM_RESERVE | MEM_COMMIT,
			PAGE_READWRITE);
		if(code == NULL)
		{
			return 3;
		}
	}

	memcpy(code, mem, ntHeader->OptionalHeader.SizeOfImage);
	headers = (PIMAGE_NT_HEADERS)(code + dosHeader->e_lfanew);
	headers->OptionalHeader.ImageBase = (DWORD)code;
#pragma endregion

#pragma region SECTIONS
	PIMAGE_SECTION_HEADER section = IMAGE_FIRST_SECTION(headers);

	for(int i = 0; i < headers->FileHeader.NumberOfSections; i++, section++)
	{
		unsigned char *sec = (unsigned char *)VirtualAlloc(code + section->VirtualAddress,
			section->SizeOfRawData,
			MEM_COMMIT,
			PAGE_READWRITE);

		memcpy(sec, mem + section->PointerToRawData, section->SizeOfRawData);
		section->Misc.PhysicalAddress = (DWORD)sec;

		{
			DWORD prot = 0;
			DWORD oldprot = 0;

			if((section->Characteristics & IMAGE_SCN_MEM_EXECUTE) != 0)
			{
				if((section->Characteristics & IMAGE_SCN_MEM_READ) != 0 && (section->Characteristics & IMAGE_SCN_MEM_WRITE) != 0)
				{
					prot = PAGE_EXECUTE_READWRITE;
				}
				else if((section->Characteristics & IMAGE_SCN_MEM_READ) != 0)
				{
					prot = PAGE_EXECUTE_READWRITE;
				}
				else if((section->Characteristics & IMAGE_SCN_MEM_WRITE) != 0)
				{
					prot = PAGE_EXECUTE_READWRITE;
				}
				else
				{
					prot = PAGE_EXECUTE;
				}
			}
			else if ((section->Characteristics & IMAGE_SCN_MEM_READ) != 0)
			{
				prot = PAGE_READONLY;
			}
			else if((section->Characteristics & IMAGE_SCN_MEM_WRITE) != 0)
			{
				prot = PAGE_WRITECOPY;
			}
			else if((section->Characteristics & IMAGE_SCN_MEM_READ) != 0 && (section->Characteristics & IMAGE_SCN_MEM_WRITE) != 0)
			{
				prot = PAGE_READWRITE;
			}
			else
			{
				prot = PAGE_NOACCESS;
			}

			if (section->Characteristics & IMAGE_SCN_MEM_NOT_CACHED != 0) {
				prot |= PAGE_NOCACHE;
			}
			
			if(!VirtualProtect((LPVOID*)section->Misc.PhysicalAddress, section->SizeOfRawData, prot, &oldprot))
				puts("Error");
			//printf("%s: 0x%.8X 0x%.8X 0x%.8X 0x%.8X\n", (char*)section->Name, 
			//												   section->Misc.PhysicalAddress, 
			//												   section->SizeOfRawData, 
			//												   prot, 
			//												   oldprot);
		}

		sections.push_back(section);
	}
#pragma endregion

#pragma region RELOCATIONS
	PIMAGE_DATA_DIRECTORY dir = &headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC];
	DWORD delta = (DWORD)(code - ntHeader->OptionalHeader.ImageBase);
	if(dir->Size > 0 && delta != 0)
	{
		for(PIMAGE_BASE_RELOCATION rel = (PIMAGE_BASE_RELOCATION)(code + dir->VirtualAddress);
			rel->VirtualAddress > 0;
			rel = (PIMAGE_BASE_RELOCATION)((char *)rel + rel->SizeOfBlock))
		{
			unsigned char *dest = code + rel->VirtualAddress;
			unsigned short *relI = (unsigned short*)((unsigned char*)rel + sizeof(IMAGE_BASE_RELOCATION));
			for(DWORD i = 0; i < (rel->SizeOfBlock-sizeof(IMAGE_BASE_RELOCATION)/2); i++, relI++)
			{
				switch(*relI >> 12)
				{
				case IMAGE_REL_BASED_ABSOLUTE:
					break;
				case IMAGE_REL_BASED_HIGHLOW:
					DWORD oldprot;
					VirtualProtect((DWORD*)(dest + (*relI & 0xfff)), 4, PAGE_READWRITE, &oldprot);
					*(DWORD*)(dest + (*relI & 0xfff)) += delta; 
					VirtualProtect((DWORD*)(dest + (*relI & 0xfff)), 4, oldprot, &oldprot);
					break;
				default:
					printf("Unknown relocation: %d\n", *relI >> 12);
					break;
				}

			}

		}

	}
#pragma endregion

#pragma region IMPORTS
	dir = &headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT];

	if(dir->Size > 0)
	{
		PIMAGE_IMPORT_DESCRIPTOR importDescr = (PIMAGE_IMPORT_DESCRIPTOR)(code + dir->VirtualAddress);

		for(; !IsBadReadPtr(importDescr, sizeof(IMAGE_IMPORT_DESCRIPTOR)) && importDescr->Name; importDescr++)
		{
			HMODULE handle = LoadLibrary((char*)code + importDescr->Name);

			DWORD* thunkRef;
			FARPROC *funcRef;

			if(importDescr->OriginalFirstThunk)
			{
				thunkRef = (DWORD*)(code + importDescr->OriginalFirstThunk);
			}
			else
			{
				thunkRef = (DWORD*)(code + importDescr->FirstThunk);
			}
			funcRef = (FARPROC*)(code + importDescr->FirstThunk);

			for(; *thunkRef; thunkRef++, funcRef++)
			{
				if(IMAGE_SNAP_BY_ORDINAL(*thunkRef))
				{
					*funcRef = GetFuncAddr(handle, (LPCSTR)IMAGE_ORDINAL(*thunkRef));
				}
				else
				{
					DWORD oldprot;
					PIMAGE_IMPORT_BY_NAME import = (PIMAGE_IMPORT_BY_NAME)(code + *thunkRef);
					VirtualProtect(funcRef, 4, PAGE_READWRITE, &oldprot);
					*funcRef = GetFuncAddr(handle, (char*)&import->Name);
					VirtualProtect(funcRef, 4, oldprot, &oldprot);
				}

			}
		}
	}
#pragma endregion

#pragma region EXECUTE_TLS
	dir = &headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS];

	if(dir->VirtualAddress > 0)
	{
		PIMAGE_TLS_DIRECTORY tls = (PIMAGE_TLS_DIRECTORY)(code + dir->VirtualAddress);
		PIMAGE_TLS_CALLBACK *callback = (PIMAGE_TLS_CALLBACK*)tls->AddressOfCallBacks;

		if(callback)
		{
			while(*callback)
			{
				(*callback)((LPVOID)code, DLL_PROCESS_ATTACH, NULL);
				printf("Calling callback 0x%.8X\n", *callback);
				callback++;
			}
		}
	}
#pragma endregion

#pragma region EXECUTE_ENTRY_POINT

	if(headers->OptionalHeader.AddressOfEntryPoint != 0)
	{
		initialized = ((BOOL (WINAPI *)(HINSTANCE, DWORD, LPVOID))(code + headers->OptionalHeader.AddressOfEntryPoint))((HINSTANCE)code, DLL_PROCESS_ATTACH, 0);
		if(!initialized)
		{
			return 0;
		}
	}

#pragma endregion
	return initialized;
}

FARPROC MemoryModule::GetProcAddress(char* name)
{
	int idx = -1;
	PIMAGE_EXPORT_DIRECTORY exports;
	PIMAGE_DATA_DIRECTORY dir = &headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];


	if(dir->Size ==0)
		return 0;

	exports = (PIMAGE_EXPORT_DIRECTORY)(code + dir->VirtualAddress);

	DWORD *nameRef = (DWORD *) (code + exports->AddressOfNames);
	WORD *ordinal = (WORD *) (code + exports->AddressOfNameOrdinals);


	for(int i = 0; i < exports->NumberOfNames; i++, nameRef++, ordinal++)
	{
		if(strcmp((const char*)(code + *nameRef), name) == 0)
		{
			idx = *ordinal;
		}
	}

	if(idx == -1)
		return NULL;


	return (FARPROC)(code + (*(DWORD*)(code + exports->AddressOfFunctions + (idx*4))));

}

HMODULE MemoryModule::GetHandle()
{
	return (HMODULE)code;
}